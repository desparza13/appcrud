import { Component, /**/OnInit } from '@angular/core';
/////
import { Task } from '../models/task';
import { TodoService } from '../services/todo.service';
/////
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
/////
export class HomePage implements OnInit {
  todos: Task[];
  constructor(private todoService: TodoService) {}
  ngOnInit() {
    this.todoService.getTodos().subscribe(res => {
      console.log('Tareas', res);
      this.todos = res;
    });
  }
}
/////
