export interface Task {
    id?:string;
    task: string;
    priority: number;
    place: string ;
    owner: string ;
}
