// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCf9BznRCuVxd2BwolsGXMfdlJi64Ltoho",
    authDomain: "apps2-b2181.firebaseapp.com",
    databaseURL: "https://apps2-b2181.firebaseio.com",
    projectId: "apps2-b2181",
    storageBucket: "apps2-b2181.appspot.com",
    messagingSenderId: "401092700252",
    appId: "1:401092700252:web:98f6278da75b4f5273de99"
    }  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
